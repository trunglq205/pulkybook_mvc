USE [Bulky2]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 8/15/2022 2:59:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[City] [nvarchar](max) NULL,
	[Discriminator] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[PostalCode] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[StreetAddress] [nvarchar](max) NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[DisplayOrder] [int] NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[StreetAddress] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[PostalCode] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CoverTypes]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CoverTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CoverTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderHeaders]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderHeaders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NOT NULL,
	[OrderDate] [datetime2](7) NOT NULL,
	[ShippingDate] [datetime2](7) NOT NULL,
	[OrderTotal] [float] NOT NULL,
	[OrderStatus] [nvarchar](max) NULL,
	[PaymentStatus] [nvarchar](max) NULL,
	[TrackingNumber] [nvarchar](max) NULL,
	[Carrier] [nvarchar](max) NULL,
	[PaymentDate] [datetime2](7) NOT NULL,
	[PaymentDueDate] [datetime2](7) NOT NULL,
	[SessionId] [nvarchar](max) NULL,
	[PaymentIntentId] [nvarchar](max) NULL,
	[City] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[PhoneNumber] [nvarchar](max) NOT NULL,
	[PostalCode] [nvarchar](max) NOT NULL,
	[State] [nvarchar](max) NOT NULL,
	[StreetAddress] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_OrderHeaders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[ISBN] [nvarchar](max) NOT NULL,
	[Author] [nvarchar](max) NOT NULL,
	[ListPrice] [float] NOT NULL,
	[Price] [float] NOT NULL,
	[Price50] [float] NOT NULL,
	[Price100] [float] NOT NULL,
	[ImageUrl] [nvarchar](max) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[CoverTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShoppingCarts]    Script Date: 8/15/2022 2:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShoppingCarts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[ApplicationUserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_ShoppingCarts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220712150612_ReCreateDatabase', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220712154712_ExtendIdentityUser', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220712160542_AddCompanyToDb', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220712170358_updateCompanyTable', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220713040833_addCompanyId', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220713055805_AddShoppingCartTable', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220713141225_AddOrderHeaderAndOrderDetail', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220713152205_AddComlumnsOrderHeader', N'6.0.6')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20220713160412_AddProductIdToOrderDetail', N'6.0.6')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'7f195e71-1890-4f1a-be57-7cab81940f84', N'Company', N'COMPANY', N'dab2004f-9d38-4a00-9d6b-85dcf52a7bba')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9723470a-94ec-4351-9e37-9987cf61600d', N'Admin', N'ADMIN', N'692435db-8003-4283-b4f7-5385af09e216')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'af70c49f-4062-4286-a606-3580178d2c0f', N'Individual', N'INDIVIDUAL', N'da009e71-22d2-4cef-a428-931549459116')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'b1247388-1d1a-4432-a6f3-21a5477f0796', N'Employee', N'EMPLOYEE', N'1fd63af0-3592-44ed-bf5e-5e3135de2618')
GO
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Facebook', N'4155640054661189', N'Facebook', N'e21094a4-afee-4001-b5f5-db015d227a39')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', N'7f195e71-1890-4f1a-be57-7cab81940f84')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'930815cd-1415-4772-885c-f55df34f145b', N'7f195e71-1890-4f1a-be57-7cab81940f84')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e2dea5f6-b9fb-4fb3-8cec-915610d1559a', N'7f195e71-1890-4f1a-be57-7cab81940f84')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', N'9723470a-94ec-4351-9e37-9987cf61600d')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2750d882-0be1-416f-8e77-e3e938abde69', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5e4a74c6-5fa0-4cd8-8f82-e2a05ee21000', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7a5a3e84-21a4-48db-b81a-bd5020798e75', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a4e7dc72-1a65-4937-af9c-f2779458a40e', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c4e86975-36f6-4632-ba9d-b761b07ec838', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e06d88b9-b7f3-470f-ac47-e3dba0c03c96', N'af70c49f-4062-4286-a606-3580178d2c0f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e21094a4-afee-4001-b5f5-db015d227a39', N'af70c49f-4062-4286-a606-3580178d2c0f')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'', N'Gbma@gmail.com', N'GBMA@GMAIL.COM', N'Gbma@gmail.com', N'GBMA@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEEuRErBkR4ghSfxMJ0aXuprp+FIDgxieYvyhj0tRQJmneRfCp8JtHRkHMOYUZNsFrQ==', N'LMQWR6Z5OSX4Z3C2LGVRYTI2OWGPMASP', N'6b86c849-2517-4058-a639-6d6fa627ba4b', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'2750d882-0be1-416f-8e77-e3e938abde69', N'testemail@gmail.com', N'TESTEMAIL@GMAIL.COM', N'testemail@gmail.com', N'TESTEMAIL@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEDGkvEcVrVQ6fczYXKfF421R1SyxkcsqZYyyVrJ3dEWtzeel6obePeDx1wsKQr8Ahg==', N'V5MIJWEJT6CZ2IK3R6VLTOAWJ46GU7YS', N'486337e2-5fb8-4362-a3d3-441f844665ad', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'emailtest', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'5e4a74c6-5fa0-4cd8-8f82-e2a05ee21000', N'admin@gmail.com.vn', N'ADMIN@GMAIL.COM.VN', N'admin@gmail.com.vn', N'ADMIN@GMAIL.COM.VN', 0, N'AQAAAAEAACcQAAAAEOfDzKBlTQo+EbOB2TAHDRWctH5U/AyEguV1mXF1cFsMaAtYgSDYrYunSn/cSh26ZA==', N'WTMPDOM2BSFJ7D64FUCOARVMLBHJ7N55', N'91de1015-ad03-47d1-9b5f-5d59f3543580', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', NULL, N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'6cc0f8c8-e46f-4ed8-9301-24d8ed5264ca', N'abvcc@gmail.com', N'ABVCC@GMAIL.COM', N'abvcc@gmail.com', N'ABVCC@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEHwR0uDs4i2Uc2lydbFx9rEoBISbB3rJKILDX3OQ/42UPiq5gein3SoOWrUhN19rWg==', N'3XDKAV6UPJ26LKHCFGKRB545J72FKGW7', N'c1d8b4e1-0b05-45bd-8550-1a2ace3ce9a8', N'5252424242', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'abc', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'7a5a3e84-21a4-48db-b81a-bd5020798e75', N'testemail23@gmail.com', N'TESTEMAIL23@GMAIL.COM', N'testemail23@gmail.com', N'TESTEMAIL23@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAECs8hgHSL1EMgYm1sMg6QdUEYPidqz/GRkCtZeAVzHQKjO0XdtQhORwcOnPYR5zYZQ==', N'ZT3G6H5TKUFPZA4AB4WVQYRJH5F6TYQK', N'7439cddc-9af8-409d-9a17-b2bc72e2e25d', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', N'testuser@gmail.com', N'TESTUSER@GMAIL.COM', N'testuser@gmail.com', N'TESTUSER@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEEBdOWC6ytCYXH6F9iiSeEVvUE9z1a0Tkp0wrNWvIOG3+hH3R/FGUhp61B+0Ycv1mA==', N'O4HBW7FZVT26HLTYO5YBMMNRIWO4TYEL', N'0364a65a-31c5-430b-82b9-ea32424cc3cd', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', 1)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'930815cd-1415-4772-885c-f55df34f145b', N'abv123@gmail.com', N'ABV123@GMAIL.COM', N'abv123@gmail.com', N'ABV123@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAELUiQQnpZMhDzFDLLzick8OpT4B0eHv+vv5iMiOwo8NOqV9/zUxMNzUJaK3fsljhnA==', N'W2CEUL6C4O4KOGPPLYUDE76736M3V6CF', N'b5cac56b-a347-4fba-b2ab-22d8eb54a5b0', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', 1)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'a4e7dc72-1a65-4937-af9c-f2779458a40e', N'testuser2@gmail.com', N'TESTUSER2@GMAIL.COM', N'testuser2@gmail.com', N'TESTUSER2@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEFObF9u2IDcYn+b1qETN67QieLfGWpUxjp93x1EpWqM4eymkF9m59L6RSePDq8w+iQ==', N'HR5SSSWFHJWURP6O5R2J74Q2M6DNAA5A', N'1531c298-f1a9-4162-9c76-14961a620224', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'b8de5987-b768-4ddf-b00c-73349fd170cc', N'trunglq205@gmail.com', N'TRUNGLQ205@GMAIL.COM', N'trunglq205@gmail.com', N'TRUNGLQ205@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEGryfNM9U5O3g5jt3DJPQyZxuBQv4ORJDPDmx22dq0BsZs3yXOyqg4MSZFiHrk2wXw==', N'CKFMP5QMQZ6YFZ3OFH75A3YBOX2FVZ2A', N'3d70bf8a-8adc-49f9-aba6-309bcfbe8a80', NULL, 0, 0, NULL, 1, 0, NULL, N'IdentityUser', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', N'testadmin@gmail.com', N'TESTADMIN@GMAIL.COM', N'testadmin@gmail.com', N'TESTADMIN@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEK3M8rxSgfEwUOEbKkElc7UUWTY4v1mIX/7+UulBKKK64F+YtsT4dMPxMjsw89r6zA==', N'BCVWMOR4B6CQNAGNZPB7NMIDPSISAGA7', N'ddc365ab-0374-4568-963d-3261284c2179', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'c4e86975-36f6-4632-ba9d-b761b07ec838', N'testemail2@gmail.com', N'TESTEMAIL2@GMAIL.COM', N'testemail2@gmail.com', N'TESTEMAIL2@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEEi+q8N9+zJw2Ldb7aq1/B3MRL7lDGkpF9miy33mQ6tX4Qw3HfG/+5pMRnzBwDFK7w==', N'AQG72MJHDMVJP2WVRISLHMYZ4BVQUXQE', N'cbcea5ea-e0b9-4f7d-8b3f-1905bc467f65', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'e06d88b9-b7f3-470f-ac47-e3dba0c03c96', N'abv@gmail.com', N'ABV@GMAIL.COM', N'abv@gmail.com', N'ABV@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEL9oTeUuzEtSlrT/eJj3LHmQiqmm9cjei8NiHizuihitHPWjatXkvGh5LBRS/+DoWQ==', N'OOW63KM7SVE4DFHTWHA7GAI7LBFLYGRA', N'd161895a-f067-41b0-b065-6dae72213ec5', N'01391499391', 0, 0, NULL, 1, 0, N'q', N'ApplicationUser', N'abv', N'q', N'q', N'q', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'e21094a4-afee-4001-b5f5-db015d227a39', N'trung4416180@gmail.com', N'TRUNG4416180@GMAIL.COM', N'trung4416180@gmail.com', N'TRUNG4416180@GMAIL.COM', 0, NULL, N'XKWJ5OPTP3DMAYCNUD7Q7PKQ2GZBEJL6', N'f7109066-bb71-4e67-a961-e66b29789d78', N'0961339500', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Quang Trung', N'8', N'08', N'Sóc Sơn', NULL)
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [City], [Discriminator], [Name], [PostalCode], [State], [StreetAddress], [CompanyId]) VALUES (N'e2dea5f6-b9fb-4fb3-8cec-915610d1559a', N'testuser3@gmail.com', N'TESTUSER3@GMAIL.COM', N'testuser3@gmail.com', N'TESTUSER3@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAELyMjBrDh+YJc1h/vikENkM8TBSrs2X+5w7VXw4YLKYDqpNmikRf//xFcWYPSxf2eQ==', N'3OVFKQ5K6PIK2ZJWQEU6YY7B6M2FTETO', N'7ba611dc-78b8-435d-8af7-149b32de0e57', N'12345678', 0, 0, NULL, 1, 0, N'Hà Nội', N'ApplicationUser', N'Lê Trung', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội', 1)
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([Id], [Name], [DisplayOrder], [CreatedDateTime]) VALUES (1, N'Category 1', 1, CAST(N'2022-07-12T22:07:46.4389555' AS DateTime2))
INSERT [dbo].[Categories] ([Id], [Name], [DisplayOrder], [CreatedDateTime]) VALUES (2, N'Category 2', 2, CAST(N'2022-07-12T22:07:54.5592122' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([Id], [Name], [StreetAddress], [City], [State], [PostalCode], [PhoneNumber]) VALUES (1, N'FPT Software', N'Hòa Lạc', N'Hà Nội', N'FPT', N'1008', N'0988654321')
SET IDENTITY_INSERT [dbo].[Companies] OFF
GO
SET IDENTITY_INSERT [dbo].[CoverTypes] ON 

INSERT [dbo].[CoverTypes] ([Id], [Name]) VALUES (1, N'Cover type 1')
SET IDENTITY_INSERT [dbo].[CoverTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderDetails] ON 

INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (1, 1, 10, 56, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (2, 1, 9, 108, 90)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (3, 2, 10, 5, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (4, 2, 12, 3, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (5, 3, 10, 3, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (6, 3, 11, 2, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (7, 4, 10, 3, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (8, 4, 11, 2, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (9, 4, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (10, 5, 10, 3, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (11, 5, 11, 2, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (12, 5, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (13, 5, 15, 50, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (14, 6, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (15, 6, 15, 50, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (16, 7, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (17, 7, 15, 50, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (18, 7, 9, 1, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (19, 8, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (20, 8, 15, 50, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (21, 8, 9, 1, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (22, 9, 16, 5, 30)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (23, 9, 13, 50, 98)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (24, 10, 16, 5, 30)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (25, 10, 13, 50, 98)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (26, 10, 12, 1, 110)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (27, 11, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (28, 12, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (29, 13, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (30, 14, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (31, 15, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (32, 16, 15, 6, 92)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (33, 17, 14, 5, 200)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (34, 18, 14, 5, 200)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (35, 19, 10, 6, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (36, 20, 10, 6, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (37, 21, 14, 5, 200)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (38, 22, 14, 5, 200)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (39, 23, 10, 5, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (40, 24, 11, 1, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (41, 25, 9, 2, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (42, 26, 9, 2, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (43, 26, 10, 1, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (44, 27, 9, 2, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (45, 27, 10, 1, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (46, 28, 9, 2, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (47, 28, 10, 1, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (48, 29, 9, 6, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (49, 29, 10, 1, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (50, 30, 11, 1, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (51, 31, 11, 2, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (52, 32, 10, 2, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (53, 33, 9, 3, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (54, 34, 9, 3, 99)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (55, 35, 11, 1, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (56, 36, 10, 1, 120)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (57, 37, 11, 3, 50)
INSERT [dbo].[OrderDetails] ([Id], [OrderId], [ProductId], [Count], [Price]) VALUES (58, 38, 10, 1, 120)
SET IDENTITY_INSERT [dbo].[OrderDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderHeaders] ON 

INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (1, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-13T23:11:21.3560524' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 15880, N'Cancelled', N'Cancelled', N'322', N'321', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trungggg', N'123456789', N'qqqq3456', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (2, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-13T23:23:18.0860435' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 930, N'Cancelled', N'Cancelled', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (3, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:19:33.5931942' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 460, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (4, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:31:28.6076466' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 570, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (5, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:46:05.7706077' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 5170, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1zPDxYP0kM5lJD8BorndkzN6A0CsbgWHtVZINfreRabRpPe3fC8hoxQNY', N'pi_3LLOcVGW8GdAYsmV1FzfV4e1', N'Hà Nội', N'Lê Trung', N'0961339500', N'2005', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (6, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:48:44.9131679' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 4710, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1uqG3OdksUuRzG3YrQ6mI9Dr0sWjSBPKQJg1CQHDafCC1N3OKqqmwL1yr', N'pi_3LLOf4GW8GdAYsmV3iFeJShg', N'Hà Nội', N'Lê Trung', N'12345678', N'123', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (7, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:51:37.6267143' AS DateTime2), CAST(N'2022-08-08T00:20:40.8039889' AS DateTime2), 4809, N'Shipped', N'Approved', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1mnqEiYCVdZSKYkqoQgoGKfZovMk6mCCQL55N2i2dsiniAG7f0oeIueAR', N'pi_3LLOhqGW8GdAYsmV0ulbYPX0', N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (8, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T16:57:25.3439254' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 4809, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (9, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T17:00:35.3892649' AS DateTime2), CAST(N'2022-08-08T00:32:05.5198794' AS DateTime2), 5050, N'Shipped', N'Approved', N'24', N'42', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1o6v1XGfGaf7FcmH4pM1ArXZ4tmOpzDXh71JdfBZIDooDzY5HbVCevds1', N'pi_3LLOqXGW8GdAYsmV1aEBRmhp', N'Hà Nội', N'Lê Trung', N'12345678', N'123', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (10, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-14T17:03:04.1243984' AS DateTime2), CAST(N'2022-08-08T00:33:54.7789574' AS DateTime2), 5160, N'Cancelled', N'Refunded', NULL, N'e2', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1YqYKgpgD93LYyxweVecYiP8wmynslmbSEl3EtKcqFUc5R83sRRoiG17V', N'pi_3LLOsvGW8GdAYsmV24h4mJ65', N'Hà Nội', N'Lê Trung', N'12345678', N'1234', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (11, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T10:10:23.4283108' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 552, N'Cancelled', N'Cancelled', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (12, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T10:14:26.3366586' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 552, N'Cancelled', N'Cancelled', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (13, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T10:17:49.0598110' AS DateTime2), CAST(N'2022-08-08T10:05:08.0589195' AS DateTime2), 552, N'Shipped', N'Approved', N'123', N'None', CAST(N'2022-08-08T10:19:12.9842501' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'ch_3LUMUmGW8GdAYsmV3W8UuGYD', NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (14, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T10:19:09.1110966' AS DateTime2), CAST(N'2022-08-08T10:23:30.0089353' AS DateTime2), 552, N'Shipped', N'Approved', N'123', N'e2', CAST(N'2022-08-08T10:23:44.9219311' AS DateTime2), CAST(N'2022-09-07T10:23:30.0090534' AS DateTime2), N'ch_3LUMZAGW8GdAYsmV2qP32AnK', NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (15, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T10:20:32.4851742' AS DateTime2), CAST(N'2022-08-08T11:03:16.5655003' AS DateTime2), 552, N'Shipped', N'ApprovedForDelayedPayment', N'32', N'32', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-09-07T11:03:16.5657812' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (16, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T16:30:34.1540032' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 552, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (17, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T16:33:19.1969572' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1000, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (18, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T16:37:21.8128221' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1000, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (19, N'e06d88b9-b7f3-470f-ac47-e3dba0c03c96', CAST(N'2022-07-15T16:41:40.8260234' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 720, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1QTlNyvsP1BDol2hbCRBNE2xpkJOu6QnOeCpBdvXT4kY2wWHfQqhhAJ98', N'pi_3LLl33GW8GdAYsmV2jrXCmG6', N'q', N'abv', N'01391499391', N'q', N'q', N'q')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (20, N'e06d88b9-b7f3-470f-ac47-e3dba0c03c96', CAST(N'2022-07-15T16:46:11.5167206' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 720, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1GVdAYnYQIc9V7TbMab3ZUOST1747GXwYUU8ghMC185HWBZsKji2YOdA6', N'pi_3LLl69GW8GdAYsmV21OjVvWs', N'q', N'abv', N'01391499391', N'q', N'q', N'q')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (21, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T16:53:32.4593049' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1000, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (22, N'930815cd-1415-4772-885c-f55df34f145b', CAST(N'2022-07-15T16:54:28.0622143' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1000, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (23, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-04T14:34:00.3435143' AS DateTime2), CAST(N'2022-08-08T10:20:49.4394018' AS DateTime2), 600, N'Shipped', N'Approved', N'321', N'None', CAST(N'2022-08-08T10:21:08.1177210' AS DateTime2), CAST(N'2022-09-07T10:20:49.4395538' AS DateTime2), N'ch_3LUMWdGW8GdAYsmV2bz8jCJg', NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (24, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-04T14:34:50.0411622' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 50, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (25, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', CAST(N'2022-08-04T14:36:31.6833868' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 198, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1f6Vt01yNcenxFu3BubMgpcLOk3eqYMJZB8dgSzGywn2S1eTYkTQRtpuv', N'pi_3LSybcGW8GdAYsmV0soFJMBC', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (26, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', CAST(N'2022-08-04T14:40:29.9659431' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 318, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1aYET6750K6dVqB2vFGtZCtwucz70LPfAZpvEEZNCoiDXIURzwRjeR9l4', N'pi_3LSyfSGW8GdAYsmV2SIIDNu4', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (27, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', CAST(N'2022-08-04T15:09:54.3328109' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 318, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1DCNzmnIy9w4yYkUJ9BhnxigCCydSo38KyfqM3uiCxg7HiYkjT4Cts8CB', N'pi_3LSz7uGW8GdAYsmV3Vz5La8t', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (28, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', CAST(N'2022-08-04T15:10:02.5199527' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 318, N'Pending', N'Pending', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1jLdT1qLb07RdSsuStALsupuemAKez7mwckyEank0bf4h4QeNGfErN3pP', N'pi_3LSz82GW8GdAYsmV0TcaVVIg', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (29, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a', CAST(N'2022-08-08T00:47:28.2608760' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 714, N'Cancelled', N'Refunded', NULL, NULL, CAST(N'2022-08-08T00:47:30.1933410' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_b1RTnV452BKr205cS9pB40Yc6Ll66MMcSt8UjYzqWbdNSGcAlo25q3QGQe', N'pi_3LUDZWGW8GdAYsmV33xe8r8C', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (30, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-08T00:59:16.9177876' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 50, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (31, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-08T10:38:31.7060701' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 100, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (32, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-08T10:39:07.3750563' AS DateTime2), CAST(N'2022-08-08T13:33:16.4781232' AS DateTime2), 240, N'Shipped', N'ApprovedForDelayedPayment', N'32', N'13', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-09-07T13:33:16.4783140' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (33, N'a4e7dc72-1a65-4937-af9c-f2779458a40e', CAST(N'2022-08-08T10:40:15.3214811' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 297, N'Pending', N'Pending', NULL, NULL, CAST(N'2022-08-08T10:40:16.9819407' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1hMW1hSC4fmBiLINKOJqOpKR165nBJE8XbbVcSpNIwDdkGSrXLIajbQp4', N'pi_3LUMpBGW8GdAYsmV3EGkYML5', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (34, N'a4e7dc72-1a65-4937-af9c-f2779458a40e', CAST(N'2022-08-08T10:40:44.4664273' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 297, N'Pending', N'Pending', NULL, NULL, CAST(N'2022-08-08T10:40:45.2069392' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1rLFdwTwBknBQdJCkx3Yh84IRqmu4RrJk7P1Bjh7RV0NivySvQXWuCIiA', N'pi_3LUMpdGW8GdAYsmV2X6ugeWD', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (35, N'a4e7dc72-1a65-4937-af9c-f2779458a40e', CAST(N'2022-08-08T10:41:35.0857830' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 50, N'Pending', N'Pending', NULL, NULL, CAST(N'2022-08-08T10:41:35.8014034' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1Jb3EdEH5ekT2xW1frfcXBY89KPGdj7C5FAO0xBAShS0mdGPlb2c5Oiza', N'pi_3LUMqSGW8GdAYsmV02rWWUrd', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (36, N'e2dea5f6-b9fb-4fb3-8cec-915610d1559a', CAST(N'2022-08-08T10:47:45.0411525' AS DateTime2), CAST(N'2022-08-08T11:15:56.5698012' AS DateTime2), 120, N'Shipped', N'ApprovedForDelayedPayment', N'423', N'42', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2022-09-07T11:15:56.5698026' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (37, N'a4e7dc72-1a65-4937-af9c-f2779458a40e', CAST(N'2022-08-08T11:06:45.0727398' AS DateTime2), CAST(N'2022-08-08T11:11:56.0015546' AS DateTime2), 150, N'Shipped', N'Approved', N'42', N'23', CAST(N'2022-08-08T11:06:46.0427990' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'cs_test_a1dzh6i5Cruk7Y3VTFLSfhi0gT45H9LdMlGftRsopZSdfiwtmNMlqvSNNA', N'pi_3LUNEoGW8GdAYsmV0z7yEvB5', N'Hà Nội', N'Lê Trung', N'12345678', N'42424', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
INSERT [dbo].[OrderHeaders] ([Id], [ApplicationUserId], [OrderDate], [ShippingDate], [OrderTotal], [OrderStatus], [PaymentStatus], [TrackingNumber], [Carrier], [PaymentDate], [PaymentDueDate], [SessionId], [PaymentIntentId], [City], [Name], [PhoneNumber], [PostalCode], [State], [StreetAddress]) VALUES (38, N'91fc0c56-bbbe-40f0-830c-98ff880b8ba1', CAST(N'2022-08-08T16:17:39.7949669' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 120, N'Approved', N'ApprovedForDelayedPayment', NULL, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'Hà Nội', N'Lê Trung', N'12345678', N'q', N'Hà Nội', N'123 Cầu Giáy, Hà Nội')
SET IDENTITY_INSERT [dbo].[OrderHeaders] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (9, N'For Whom the Bell Tolls', N'<p>abc</p>', N'1234', N'Ernest Hemingway', 100, 99, 98, 90, N'\images\products\7f242c71-a164-4ba4-912e-2d90e699ad0c.jpg', 1, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (10, N'Bartleby The Scrivene', N'<p>abc</p>', N'2312', N'Herman Melville', 123, 120, 110, 105, N'\images\products\cb6f3471-60a2-4259-af41-6a82336c7825.jpg', 2, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (11, N'Cosmos', N'<p>adac</p>', N'422', N'Carl Sagan', 55, 50, 49, 45, N'\images\products\8defe481-438d-479a-aa81-a00f85c69af0.jpg', 2, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (12, N'Man''s Search for Meaning', N'<p>af23f3f</p>', N'312', N'Joe Haldeman', 111, 110, 109, 108, N'\images\products\3ed37d13-6d21-43f5-bf1a-ca41ed970ece.jpg', 1, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (13, N'A Short History of Nearly Everything', N'<p>qư13</p>', N'3121', N'Bill Bryson', 99, 98, 97, 95, N'\images\products\44ee86de-0075-4dc7-8f83-fdd58f1a0389.jpg', 2, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (14, N'The Last Lecture', N'<p>43fff</p>', N'3212', N'Randy Pausch', 210, 200, 190, 170, N'\images\products\67291268-5624-4e47-9bb4-e53b859b8aab.jpg', 2, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (15, N'Watership Down', N'<p>trew</p>', N'4321', N'Richard Adams', 100, 92, 85, 80, N'\images\products\005e7ffa-1348-49de-b5dd-88afb8a52cb7.jpg', 1, 1)
INSERT [dbo].[Products] ([Id], [Title], [Description], [ISBN], [Author], [ListPrice], [Price], [Price50], [Price100], [ImageUrl], [CategoryId], [CoverTypeId]) VALUES (16, N'The Forever War ', N'<p>3f</p>', N'1312', N'Viktor Frankl', 32, 30, 28, 25, N'\images\products\72858996-3a28-4b50-9d10-364003cd9682.jpg', 1, 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[ShoppingCarts] ON 

INSERT [dbo].[ShoppingCarts] ([Id], [ProductId], [Count], [ApplicationUserId]) VALUES (18, 10, 6, N'e06d88b9-b7f3-470f-ac47-e3dba0c03c96')
INSERT [dbo].[ShoppingCarts] ([Id], [ProductId], [Count], [ApplicationUserId]) VALUES (38, 12, 1, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a')
INSERT [dbo].[ShoppingCarts] ([Id], [ProductId], [Count], [ApplicationUserId]) VALUES (39, 11, 1, N'b96d3075-f6a7-4dcc-9720-1913c97fb82a')
SET IDENTITY_INSERT [dbo].[ShoppingCarts] OFF
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT (N'') FOR [Discriminator]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [City]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [Name]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [PhoneNumber]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [PostalCode]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [State]
GO
ALTER TABLE [dbo].[OrderHeaders] ADD  DEFAULT (N'') FOR [StreetAddress]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Companies_CompanyId] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_Companies_CompanyId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_OrderHeaders_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[OrderHeaders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_OrderHeaders_OrderId]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Products_ProductId]
GO
ALTER TABLE [dbo].[OrderHeaders]  WITH CHECK ADD  CONSTRAINT [FK_OrderHeaders_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderHeaders] CHECK CONSTRAINT [FK_OrderHeaders_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories_CategoryId] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories_CategoryId]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_CoverTypes_CoverTypeId] FOREIGN KEY([CoverTypeId])
REFERENCES [dbo].[CoverTypes] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_CoverTypes_CoverTypeId]
GO
ALTER TABLE [dbo].[ShoppingCarts]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCarts_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShoppingCarts] CHECK CONSTRAINT [FK_ShoppingCarts_AspNetUsers_ApplicationUserId]
GO
ALTER TABLE [dbo].[ShoppingCarts]  WITH CHECK ADD  CONSTRAINT [FK_ShoppingCarts_Products_ProductId] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ShoppingCarts] CHECK CONSTRAINT [FK_ShoppingCarts_Products_ProductId]
GO
